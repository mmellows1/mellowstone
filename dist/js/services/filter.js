app.filter('count', function() {
	return function(item) {
		var count = 0; 
		angular.forEach(item, function(value, key) {
			count += 1;
		})
		return count;
	}
})

app.filter('percentage', function() {
	return function(item, main) {
		var count = 1; 
		var count2 = 1;
		angular.forEach(item, function(value, key) {
			count += 1;
		})
		angular.forEach(main, function(value, key) {
			count2 += 1;
		})
		var max = count2;
		var only = count;
		var sum = 100 / max;
		var sum2 = sum * only;
		return sum2;
	}
})