app.service('Database', ['$rootScope', '$http', '$firebaseObject', '$firebaseArray', '$firebaseAuth', 'Cache',
	function($rootScope, $http, $firebaseObject, $firebaseArray, $firebaseAuth, Cache) {
		return {
			init: function() {
				var ref = firebase.database().ref();
				var data = $firebaseObject(ref);
				$rootScope.loading = true;
				data.$loaded().then(function() {
					$rootScope.ExcelCopy = data;
					$rootScope.loading = false;
				});	
			},
			signin: function() {
				$firebaseAuth().$signInWithPopup("google").then(function(result) {
					Cache.set(result.user, 'user');
					$rootScope.user = result.user;
				});
			},
			signout: function() {
				Cache.remove('user');
			},
			getTranslation: function(code) {
				var ref = firebase.database().ref();
				return $firebaseObject(ref.child(code))
			},
			getSentences: function(code) {
				var ref = firebase.database().ref();
				return $firebaseObject(ref.child(code).child('sentences'));
			},
			save: function() {
				$rootScope.ExcelCopy.$save();
			}
		}
	}
]);