app.service('Cache', ['$localStorage', 
	function($localStorage) {
		return {
			get: function(name) {
				return $localStorage[name];
			},
			set: function(item, name) {
				return $localStorage[name] = item;
			},
			remove: function(name) {
				delete $localStorage[name];
			}
		}
	}
])