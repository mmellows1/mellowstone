app.service('Helper', [
	function() {
		return { 
			convertObjectToArray: function(object) {
				var arr = [];
				angular.forEach(object, function(key, value) {
					arr.push([value, key])
				})
				return arr;
			}
		}
	}
])