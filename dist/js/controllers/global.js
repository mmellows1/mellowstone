app.controller('GlobalController', ['$scope', '$rootScope', 'FileUploader', '$http', 'Cache', 'Helper', 'Database', '$firebaseAuth', '$timeout',
	function($scope, $rootScope, FileUploader, $http, Cache, Helper, Database, $firebaseAuth, $timeout) {
	
		// =============================
		// User Auth
		// =============================
		$scope.authObj = $firebaseAuth();
		$scope.signin = function() {
			$scope.authObj.$signInWithPopup("google").then(function(result) {
				$scope.user = result.user;
			});
		}

		$scope.signout = function() {
			$scope.authObj.$signOut();
		}

		$scope.authObj.$onAuthStateChanged(function(firebaseUser) {
		  	if (firebaseUser) {
		   	 	Database.init()
		   	 	$scope.user = $scope.authObj.$getAuth();
		  	} else {
		  		$scope.user = null;
		  	}
		});

	    $scope.save = function() {
	    	var option = confirm("Are you sure you want to save this option?");
	    	if(option == true) {
	    		Database.save();
		    	$timeout(function() {
		    		$rootScope.alert('alert-success', 'Your data has been saved');
		    	}, 500)
	    	} else {
	    		return false;
	    	}
	    	
	    }

	    $scope.delete = function(key) {
	    	var option = confirm("Are you sure you want to delete this language?");
	    	if(option == true) {
	    		delete $rootScope.ExcelCopy[key];
	    	} else {
	    		return false;
	    	}
	    	
	    }

	    $rootScope.alert = function(classs, message) {
	    	$scope.alertShow = true;
	    	$scope.alertClass = classs;
	    	$scope.alertMessage = message;
	    }

	    $scope.defaultLanguage = 'English';
	    $scope.setDefaultLanguage = function(language) {
	    	$scope.defaultLanguage = language;
	    }

		$scope.exists = function(needle, haystack) {
			var exists = false;
			for(var i = 0; i < haystack.length; i++) {
				if(needle === haystack[i]) {
					exists = true;
				}
			}
			return exists;
		}

		$scope.countRows = function(rows, column) {
			var count = 0;
			
			angular.forEach(rows, function(value, key) {
				// console.log(rows, column, value, key);	
				if(key !=='!ref' && key !== '!margins') {
					var letter = key.charAt(0);
					var number = key.charAt(1);
					if(letter === column) {
						count += 1;
					}
				}
			})
			return count;
		}

		$scope.read = function(workbook) {
	    	$rootScope.Excel = workbook;
	    	console.log(workbook);
	    	$rootScope.ExcelCopy = {};
	    	$rootScope.FinalExcelCopy = {};
	    	var columns = {};
			$timeout(function() {
				// STEP 1. Convert the main excel into 2 columns for each language/sheet
				angular.forEach($rootScope.Excel.Sheets, function(value, key) {
					$rootScope.ExcelCopy[key] = {};
					$rootScope.ExcelCopy[key].A = [];
					angular.forEach(value, function(childValue, childKey) {
						if(childKey !== '!margins' && childKey !== '!ref') {
							var columnLetter = childKey.charAt(0);
							var columnNumber = childKey.charAt(1);
							if(!$scope.exists(columnLetter, columns)) {
							 	columns[columnLetter] = [];
							 	angular.forEach(value, function(childChildValue, childChildKey) {
							 		// console.log(childChildValue);
							 		if(childChildKey.charAt(0) === columnLetter) {
							 			if(childChildValue.v) {
							 				var newValue = childChildValue.v;
							 					newValue = newValue.toString();
							 				columns[columnLetter].push(newValue);	
							 			} else {
							 				var newValue = childChildValue.h;
							 					newValue = newValue.toString();
							 				columns[columnLetter].push(newValue);
							 			}
							 		}
							 	})
							}
							$rootScope.ExcelCopy[key] = columns;
						}

					})
					console.log($rootScope.ExcelCopy);
				})

				// STEP 2. Assign the keys
				angular.forEach($rootScope.ExcelCopy, function(value, key) {
					angular.forEach(value, function(childValue, childKey) {
						var language = $rootScope.ExcelCopy[key][childKey][0];
						if(childKey !== 'A') {
							$rootScope.FinalExcelCopy[language] = {};
						}
						for(var i = 1; i < childValue.length; i++) {
							if(childKey !== 'A') {
								var variable = $rootScope.ExcelCopy[key]['A'][i];
								var translation = $rootScope.ExcelCopy[key][childKey][i];
								$rootScope.FinalExcelCopy[language][variable] = translation;
							}
						} 
					})
				}) 
				// STEP 3. Delete the variable from the final object
				$rootScope.ExcelCopy = $rootScope.FinalExcelCopy;
			}, 300);
		}
    }
])