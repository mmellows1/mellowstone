var app = angular.module('TranslationApp', ['angularFileUpload', 'ngCsvImport', 'ngSanitize', 'ngCsv', 'ngStorage', 'firebase', 'angular-js-xlsx']);

// =============================
// Firebase Config
// =============================
var config = {
    apiKey: "AIzaSyDVpFvuMAv3lb3FKWhXbzu-yuYr22VFQ3s",
    authDomain: "translations-generator.firebaseapp.com",
    databaseURL: "https://translations-generator.firebaseio.com",
    projectId: "translations-generator",
    storageBucket: "translations-generator.appspot.com",
    messagingSenderId: "1054795345481"
 };
 
firebase.initializeApp(config);